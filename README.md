**This script ( install.sh ) install OpenVpn & IPsec on Linux/Unix server and configure them to work as a VPN server.**

**OpenVpn** works with clients on **OS Windows**, **Mac OS**, **Android**. **IPsec** works with clients on **Mac OS** & **IOS** **(IKev1)**

---

## 1. Recommended system requirements:

- OS Unix / Linux ( **Ubuntu 14 /16 , Debian 7 / 8 / 9** )
- Internet access on server
- At least **1024 MB RAM**
- At least **5 GB Disk** space 
- Clean system (There are should be no other installed packets on server)
- Root access to server ( Login / Password / Port )

---

## Packets for installation:

- **Common Packets** :
	- curl
	- pwgen
	- python-pip
	- python-dev
	- openssl
	- build-essential
	- autoconf
	- libtool
	- pkg-config
	- ppp
	- xl2tpd
	- make
	- grepcidr


- **OpenVpn Packets** :
	- openvpn
	- easy-rsa


- **IPsec Packets** :
	- strongswan  (Debian 7/8/9)
	- libcharon-extra-plugins  (Debian 7/8/9)
	- strongswan-plugin-xauth-generic  (Ubuntu 14/16)


## Additional system changes:

- Firewall rule for traffic routing (Iptables)
- systemctl.conf changes ( net.ipv4.ip_forward=1 )

---

## Usage instruction:

1. Download and run script on your server : 

`wget -O - https://bitbucket.org/snowdvpn/private-vpn-script/raw/master/install.sh | bash`

2. After script will finish his intallation You will see a rows with login, password, secret phrase(IPsec) and OpenVpn config

*Installation **Log file** --> **/root/install.log***

*For connect via Snowd please use this **configurating file** --> **/etc/openvpn/client-config.txt***

*New User was generated:*  

**Username** "xxxxxxxx" **Password** "xxxxxxxx" **SecretPhrase for Ipsec** "xxxxxxxx"

3. Also You can download another script  **useradd.sh** :

`wget https://bitbucket.org/snowdvpn/private-vpn-script/raw/master/useradd.sh`

You can use this script for automatical user creation. Usage :

`sudo bash /root/useradd.sh`