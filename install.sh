#!/bin/bash

echo  "$(tput setaf 5)Updating system. Please wait ! $(tput sgr0)"
###Prepare system

#Ipsec variables
IPSec=0         #IPSec 1 Start IP Subnet
i=4  		    #variable for configs IPs increase

#Openvpn variables 
TCP=3   # tcp openvpn
UDP=2   # udp openvpn
IPT=0   # iptables

#Installation status Log
Log=/root/install.log
touch /root/install.log

#clean configs before install
rm -rf /etc/ipsec.conf
rm -rf /etc/ipsec.secrets
cat /dev/null > /etc/rc.local

#flush iptables rules
iptables -t nat -F
iptables -F

#private network
NETWORKS="10.0.0.0/8 172.16.0.0/12 192.168.0.0/16"

###Path for easy-rsa certificates
CertPathD9=/usr/share/easy-rsa
CertPathD8=/usr/share/easy-rsa
CertPathD7=/usr/share/doc/openvpn/examples/easy-rsa/2.0
CertPathU16=/usr/share/easy-rsa
CertPathU14=/usr/share/easy-rsa

###Packets for certificates
CertPackD9="openvpn easy-rsa"
CertPackD8="openvpn easy-rsa"
CertPackD7="openvpn"
CertPackU16="openvpn easy-rsa"
CertPackU14="openvpn easy-rsa"

###Packets for IPsec (strongswan)
IpsecPackD9="apt-get -y install strongswan libcharon-extra-plugins"
IpsecPackD8="apt-get -y install strongswan libcharon-extra-plugins"
IpsecPackD7="apt-get -y -t wheezy-backports install strongswan libcharon-extra-plugins"
IpsecPackU16="apt-get -y install strongswan strongswan-plugin-xauth-generic"
IpsecPackU14="apt-get -y install strongswan strongswan-plugin-xauth-generic"

### OpenVpn radius plugin
RadiusPluginD9="/usr/lib/openvpn/openvpn-plugin-auth-pam.so"
RadiusPluginD8="/usr/lib/openvpn/openvpn-plugin-auth-pam.so"
RadiusPluginD7="/usr/lib/openvpn/openvpn-auth-pam.so"
RadiusPluginU16="/usr/lib/openvpn/openvpn-plugin-auth-pam.so"
RadiusPluginU14="/usr/lib/openvpn/openvpn-plugin-auth-pam.so"

PreAptD7="apt-get -y install debian-keyring debian-archive-keyring"
PreAptOther="echo -e '\nplease wait!\n'"

function getOS {
if (cat /etc/issue | grep Debian | grep 9 &> /dev/null)
	then
	    OS=Debian9
	    CertPath=$CertPathD9
	    CertPack=$CertPackD9
	    IpsecPack=$IpsecPackD9
	    RadiusPlugin=$RadiusPluginD9
	    PreApt=$PreAptOther
	    
	  	MAIN

elif (cat /etc/issue | grep Debian | grep 8 &> /dev/null)
	then
	    OS=Debian8
	    CertPath=$CertPathD8
	    CertPack=$CertPackD8
	    IpsecPack=$IpsecPackD8
	    RadiusPlugin=$RadiusPluginD8
	    PreApt=$PreAptOther
	    
		MAIN

elif (cat /etc/issue | grep Debian | grep 7 &> /dev/null)
	then
	    OS=Debian7
	    CertPath=$CertPathD7
	    CertPack=$CertPackD7
	    IpsecPack=$IpsecPackD7
	    RadiusPlugin=$RadiusPluginD7
	    PreApt=$PreAptD7
	    
	    echo "deb http://ftp.debian.org/debian wheezy-backports main" > /etc/apt/sources.list.d/wheezy-backports.list
	    
		MAIN

elif (cat /etc/issue | grep Ubuntu | grep 14 &> /dev/null)
	then
	    cat /dev/null > /etc/apt/sources.list
        cat >> /etc/apt/sources.list <<EOF
        deb http://archive.ubuntu.com/ubuntu/ trusty main restricted
        deb-src http://archive.ubuntu.com/ubuntu/ trusty main restricted

        deb http://archive.ubuntu.com/ubuntu/ trusty-updates main restricted
        deb-src http://archive.ubuntu.com/ubuntu/ trusty-updates main restricted

        deb http://archive.ubuntu.com/ubuntu/ trusty universe
        deb-src http://archive.ubuntu.com/ubuntu/ trusty universe
        deb http://archive.ubuntu.com/ubuntu/ trusty-updates universe
        deb-src http://archive.ubuntu.com/ubuntu/ trusty-updates universe

        deb http://archive.ubuntu.com/ubuntu/ trusty multiverse
        deb-src http://archive.ubuntu.com/ubuntu/ trusty multiverse
        deb http://archive.ubuntu.com/ubuntu/ trusty-updates multiverse
        deb-src http://archive.ubuntu.com/ubuntu/ trusty-updates multiverse

        deb http://archive.ubuntu.com/ubuntu/ trusty-backports main restricted universe multiverse
        deb-src http://archive.ubuntu.com/ubuntu/ trusty-backports main restricted universe multiverse

        deb http://security.ubuntu.com/ubuntu trusty-security main restricted
        deb-src http://security.ubuntu.com/ubuntu trusty-security main restricted
        deb http://security.ubuntu.com/ubuntu trusty-security universe
        deb-src http://security.ubuntu.com/ubuntu trusty-security universe
        deb http://security.ubuntu.com/ubuntu trusty-security multiverse
        deb-src http://security.ubuntu.com/ubuntu trusty-security multiverse
EOF

	    OS=Ubuntu14
	    CertPath=$CertPathU14
	    CertPack=$CertPackU14
	    IpsecPack=$IpsecPackU14
	    RadiusPlugin=$RadiusPluginU14
	    PreApt=$PreAptOther
	    
		MAIN

elif (cat /etc/issue | grep Ubuntu | grep 16 &> /dev/null)
	then
	    OS=Ubuntu16
	    CertPath=$CertPathU16
	    CertPack=$CertPackU16
	    IpsecPack=$IpsecPackU16
	    RadiusPlugin=$RadiusPluginU16
	    PreApt=$PreAptOther
	    
		MAIN
fi
}

function MAIN {
echo "$OS detected!"


###Updating system

$PreApt
dpkg --configure -a
apt-get update |  while read n; 
    do
	echo $n &>> $Log && echo -ne "......"
    done

echo " "

###Packets install
echo  "$(tput setaf 5)Installing common packages $(tput sgr0)"

DEBIAN_FRONTEND=noninteractive apt-get install -y pwgen nano git python-pip python-dev tmux openssl build-essential autoconf libtool pkg-config ppp xl2tpd make grepcidr |  while read n; 
    do
	echo $n &>> $Log && echo -ne "......"
    done
    
echo " "    
###Certificates generating
echo  "$(tput setaf 5)Generating certificates $(tput sgr0)"

apt-get install -y $CertPack  |  while read n; 
    do
	echo $n &>> $Log && echo -ne "......"
    done

mkdir -p $CertPath/keys
egrep -lRZ 'export KEY_ORG="Fort-Funston"' $CertPath/vars | xargs -0 -l sed -i -e 's|export KEY_ORG="Fort-Funston"|export KEY_ORG="Snowd"|g' &>> $Log
egrep -lRZ 'export KEY_EMAIL="me@myhost.mydomain"' $CertPath/vars | xargs -0 -l sed -i -e 's|export KEY_EMAIL="me@myhost.mydomain"|export KEY_EMAIL="info@snowd.com"|g' &>> $Log
egrep -lRZ 'export KEY_OU="MyOrganizationalUnit"' $CertPath/vars | xargs -0 -l sed -i -e 's|export KEY_OU="MyOrganizationalUnit"|export KEY_OU="Snowd"|g' &>> $Log
egrep -lRZ 'export KEY_NAME="EasyRSA"' $CertPath/vars | xargs -0 -l sed -i -e 's|export KEY_NAME="EasyRSA"|export KEY_NAME="server"|g' &>> $Log
egrep -lRZ 'export KEY_SIZE=1024' $CertPath/vars | xargs -0 -l sed -i -e 's|export KEY_SIZE=1024|export KEY_SIZE=2048|g' &>> $Log
cp $CertPath/openssl-1.0.0.cnf $CertPath/openssl.cnf &>> $Log

cd  $CertPath ; source vars &>> $Log ; ./clean-all &>> $Log ; ./build-ca --batch &>> $Log ; ./build-key-server --batch server &>> $Log ; ./build-dh --batch

#copy certs
mkdir -p /etc/openvpn                       &>> $Log
cp $CertPath/keys/ca.crt      /etc/openvpn/ &>> $Log
cp $CertPath/keys/dh2048.pem  /etc/openvpn/ &>> $Log
cp $CertPath/keys/server.crt  /etc/openvpn/ &>> $Log
cp $CertPath/keys/server.key  /etc/openvpn/ &>> $Log
cp $CertPath/keys/ca.crt      /etc/openvpn/ &>> $Log

###IPSEC block
echo -e "\n"

#Install Strongswan and plugins
DEBIAN_FRONTEND=noninteractive $IpsecPack |  while read n; 
    do
	echo $n &>> $Log && echo -ne "......"
    done
    
    
#name of the main interface
Int=$(ls /sys/class/net | grep -E "ens|eth0")

# Pool of servers IPs
Server_IPs=$( ip a | grep "$Int" | grep -v "mtu"| grep -v "default" |   awk '{print $2}' | awk -F '/' '{print $1}')

cat /dev/null > /etc/ipsec.conf
cat /dev/null > /etc/ipsec.secrets

#Ipsec secret generation
Secret=$(pwgen 10 1)

function ipsecConfig {
cat >> /etc/ipsec.conf <<EOF
conn $1_v1
    authby=secret
    rekeymargin=3m
    keyingtries=1
    keyexchange=ikev1
    leftfirewall=yes
    rekey=no
    left=$1
    leftsubnet=0.0.0.0/0
    leftauth=psk
    rightsubnet=10.0.$2.0/24
    rightsourceip=10.0.$2.2/24
    rightdns=8.8.8.8
    right=%any
    rightauth=psk
    rightauth2=xauth
    dpdaction=hold
    ikelifetime=12h
    dpddelay=12h
    dpdtimeout=5s
    auto=add

EOF

echo   ''$1' %any : PSK '$3''   >>  /etc/ipsec.secrets
}

for IP in $Server_IPs 
        do
                grepcidr "$NETWORKS" <( echo "$IP" ) > /dev/null  || ipsecConfig $IP $IPSec $Secret
                IPSec=$(($IPSec+$i))
        done

#additional ipsec.conf string
sed -i '1iconfig setup\' 	     /etc/ipsec.conf    &>> $Log
sed -i '2i\    uniqueids=never\' /etc/ipsec.conf    &>> $Log


###OpenVPN block
#build ta.key
openvpn --genkey --secret /etc/openvpn/ta.key       &>> $Log

function openVpnConfig {
#create TCP config
touch /etc/openvpn/"$1"_server_tcp.conf             &>> $Log
cat /dev/null > /etc/openvpn/"$1"_server_tcp.conf

cat > /etc/openvpn/"$1"_server_tcp.conf <<EOF
local $1
port 443
proto tcp
dev tun
ca ca.crt
cert server.crt
key server.key
dh dh2048.pem
server 10.0.$2.0 255.255.255.0
plugin $5 login
username-as-common-name
client-to-client
keepalive 10 120
client-cert-not-required
auth MD5
comp-lzo
user nobody
group nogroup
persist-key
persist-tun
status openvpn-status.log
verb 3
mute 20
tcp-nodelay
duplicate-cn
tls-auth ta.key 0
fragment 0
mssfix 0
sndbuf 0
rcvbuf 0
push "sndbuf 393216"
push "rcvbuf 393216"
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 8.8.8.8"
push "dhcp-option DNS 8.8.4.4"
EOF

#create UDP config
touch /etc/openvpn/"$1"_server_udp.conf
cat /dev/null > /etc/openvpn/"$1"_server_udp.conf

cat > /etc/openvpn/"$1"_server_udp.conf <<EOF
local $1
port  1194
proto udp
dev tun
ca ca.crt
cert server.crt
key server.key
dh dh2048.pem
server 10.0.$3.0 255.255.255.0
plugin $5 login
username-as-common-name
client-to-client
keepalive 10 120
client-cert-not-required
auth MD5
comp-lzo
user nobody
group nogroup
persist-key
persist-tun
status openvpn-status.log
verb 3
mute 20
tcp-nodelay
duplicate-cn
tls-auth ta.key 0
fragment 0
mssfix 0
sndbuf 0
rcvbuf 0
push "sndbuf 393216"
push "rcvbuf 393216"
push "redirect-gateway def1 bypass-dhcp"
push "dhcp-option DNS 8.8.8.8"
push "dhcp-option DNS 8.8.4.4"
EOF

### iptables rules
iptables -t nat -A POSTROUTING -s 10.0.$4.0/22 -j SNAT --to-source $1

### /etc/rc.local change
echo 'iptables -t nat -A POSTROUTING -s 10.0.'$4'.0/22 -j SNAT --to-source '$1''  >> /etc/rc.local
}

for IP in $Server_IPs
        do
                grepcidr "$NETWORKS" <( echo "$IP" ) > /dev/null  || openVpnConfig $IP $TCP $UDP $IPT $RadiusPlugin
                TCP=$(($TCP+$i))
                TCP=$(($UDP+$i))
                IPT=$(($IPT+$i))
        done

# finish change /etc/rc.local
echo "exit 0" >> /etc/rc.local                  &>> $Log
sed -i '1s|^|#!/bin/bash\n|' /etc/rc.local      &>> $Log
chmod +x /etc/rc.local                          &>> $Log

# sysctl config
echo 1 > /proc/sys/net/ipv4/ip_forward
egrep -lRZ '#net.ipv4.ip_forward=1' /etc/sysctl.conf | xargs -0 -l sed -i -e 's|#net.ipv4.ip_forward=1|net.ipv4.ip_forward=1|g'     &>> $Log
egrep -lRZ '#AUTOSTART="all"' /etc/default/openvpn | xargs -0 -l sed -i -e 's|#AUTOSTART="all"|AUTOSTART="all"|g'                   &>> $Log

#restart  Services
systemctl daemon-reload         &>> $Log
/etc/init.d/openvpn restart     &>> $Log 
update-rc.d openvpn defaults    &>> $Log 
service strongswan restart      &>> $Log  

#Function MAIN Finished
}

getOS

###Finish
Int=$(ls /sys/class/net | grep -E "ens|eth0")

MainIP=$(ip a | grep "$Int" | grep global | grep -v "$Int": | awk '{print $2}' | awk -F '/' '{print $1}')

touch /etc/openvpn/client-config.txt
cat /dev/null > /etc/openvpn/client-config.txt

function config {
cat > /etc/openvpn/client-config.txt <<EOF
client
auth-user-pass
dev tun
resolv-retry 4
nobind
persist-key
comp-lzo
verb 4
auth MD5
dhcp-option DNS 8.8.8.8
dhcp-option DNS 8.8.4.4
key-direction 1

remote $1 443 tcp
remote $1 1194 udp

<ca>
</ca>
<tls-auth>
</tls-auth>
EOF
}

for IP in $MainIP
	do
                grepcidr "$NETWORKS" <( echo "$IP" ) > /dev/null  || config  $IP
    done


#Add user for OpenVpn and IpSec
sed -i -e '/<ca>/r /etc/openvpn/ca.crt' /etc/openvpn/client-config.txt          &>> $Log  
sed -i -e '/<tls-auth>/r /etc/openvpn/ta.key' /etc/openvpn/client-config.txt    &>> $Log  


###Users creation
User=$(pwgen 10 1)
Password=$(pwgen 10 1)

echo  "$User : XAUTH  $Password" >> /etc/ipsec.secrets                          &>> $Log

useradd --shell=/bin/nologin -p $Password $User                                 &>> $Log
echo -e "$Password\n$Password" | (passwd $User)
 

service strongswan restart || service ipsec restart  || ipsec restart                                                  &>> $Log


echo "$(tput setaf 3)Installation Log file --> $(tput setaf 5)$Log $(tput sgr0)"
echo "$(tput setaf 3)For connect via Snowd please use this configurating file --> $(tput setaf 5)/etc/openvpn/client-config.txt $(tput sgr0)"
echo "$(tput setaf 3)New User was generated:  $(tput sgr0)"
echo "$(tput setaf 3)Username "$(tput setaf 5)"$User"$(tput setaf 3)" Password "$(tput setaf 5)"$Password"$(tput setaf 3)" SecretPhrase for Ipsec "$(tput setaf 5) "$Secret" $(tput sgr0)""


rm -- /root/$0  ## Script deleting 
