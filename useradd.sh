#!/bin/bash

###Users creation
User=$(pwgen 10 1)
Password=$(pwgen 10 1)

useradd --shell=/bin/nologin -p $Password $User
echo -e "$Password\n$Password" | (passwd $User)
 
echo  ""$User" : XAUTH  \"$Password\" " >> /etc/ipsec.secrets

service strongswan restart || service ipsec restart || ipsec restart

echo "$(tput setaf 3)New User was generated:  $(tput sgr0)"
echo "$(tput setaf 3)Username= $(tput setaf 5)"$User"$(tput setaf 3) Password= $(tput setaf 5)"$Password"$(tput setaf 3)"